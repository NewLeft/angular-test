import { Component } from '@angular/core';

@Component({
  selector: 'login-component',
  templateUrl: './login-component.component.html',
  styleUrls: ['./login-component.component.css']
})
export class LoginComponentComponent {
    username: string;
    password: string;
    message: string = '';
    
    clicked(event) {
        if (this.username === 'steven' && this.password === 'pass1234') {
            this.message = '登入成功';
        }
        else {
            this.message = '帳密錯誤';
        }
    }
}
