import { Component } from '@angular/core';
import { LoginComponentComponent } from './components/login-component/login-component.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

}